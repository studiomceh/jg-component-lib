import { shallowMount } from '@vue/test-utils';
import Validation from '@/lib-components/form/validation/Validation'
import Note from '@/lib-components/form/note/Note'
import 'regenerator-runtime/runtime'

let wrapper = null;

beforeEach(() => {
    wrapper = shallowMount(Validation, {
        propsData: {
            valid: true,
            message: 'validation message',
            note: 'note text'
        }
    });
});

afterEach(() => {
    wrapper.destroy();
});

describe('Validation', () => {
    test("snapshot", () => {
        const snapShot = shallowMount(Validation);
        expect(snapShot).toMatchSnapshot();
    });
    test("rendering of validation message", async () => {
        wrapper.setProps({valid: false});
        await wrapper.vm.$nextTick();
        expect(wrapper.text()).toContain("validation message");   
    });
    test("note prop to Note component", async () => {
        const note = wrapper.findComponent(Note);
        expect(wrapper.props('note')).toBe('note text');   
        expect(note.exists()).toBe(true);
    });
});