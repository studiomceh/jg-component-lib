import { mount } from '@vue/test-utils';
import Note from '@/lib-components/form/note/Note.vue';

let wrapper = null;

beforeEach(() => {
    wrapper = mount(Note, {
        propsData: {
            note: 'Note text'
        }
    });
});

afterEach(() => {
    wrapper.destroy();
});

describe("Note", () => {
    it("renders note text", () => {
        expect(wrapper.text()).toContain('Note text');
    });
});
