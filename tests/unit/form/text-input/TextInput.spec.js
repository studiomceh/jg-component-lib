import { shallowMount } from '@vue/test-utils';
import 'regenerator-runtime/runtime';
import TextInput from '@/lib-components/form/text-input/TextInput';
import Validation from '@/lib-components/form/validation/Validation'

let wrapper = null;

beforeEach(() => {
    wrapper = shallowMount(TextInput, {
        mocks: {
            $attrs
        }
    });
});

afterEach(() => {
    wrapper.destroy();
});

describe('TextInput', () => {
    test('props exists', async () => {
        wrapper.setProps({
            importedData: 'importedData',
            icon: 'icon',
            loading: false,
            showValidation: true,
            validationMessage: 'validation message'
        });
        await wrapper.vm.$nextTick();
        expect(wrapper.props('importedData')).toBe('importedData');
        expect(wrapper.props('icon')).toBe('icon');
        expect(wrapper.props('loading')).toBe(false);
        expect(wrapper.props('showValidation')).toBe(true);
        expect(wrapper.props('validationMessage')).toBe('validation message');
        wrapper.setProps({
            importedData: 1
        });
        await wrapper.vm.$nextTick();
        expect(wrapper.props('importedData')).toBe(1);
    });

    test("show validation", async () => {
        const validation = wrapper.findComponent(Validation);
        expect(validation.exists()).toBe(true);
        wrapper.setProps({showValidation: false});
        await wrapper.vm.$nextTick();
        expect(validation.exists()).toBe(false);
    });

    test("show label", async () => {
        wrapper.setData({showLabel: true});
        await wrapper.vm.$nextTick();
        const inputLabel = wrapper.find('.jngr-input-label');
        expect(inputLabel.exists()).toBe(true);
    });
    
    test("v-model", async () => {
        wrapper.setData({inputValue: 'input value'});
        await wrapper.vm.$nextTick();
        
        const inputValue = wrapper.find('input').element.value;
        expect(inputValue).toBe('input value');
        
        const input = wrapper.find('input');
        await input.setValue('input value setted');
        expect(input.element.value).toBe('input value setted');
    });

    test("attrs", () => { 
        expect($attrs).toBe(true);
    });
});